using UnityEngine;

public class Circle : MonoBehaviour
{
    public int PaintedOverTilesAmount { get; set; }

    [SerializeField] private float _speed;
    private Camera _camera;
    private Vector3 _acceleration;
    private Rigidbody2D _rigidbody;
    private Bounds _bounds;

    void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _camera = Camera.main;
        _bounds = GameManager.Instance.Bounds;
    }
    private void OnEnable() 
    {
        GameManager.Instance.OnFixedUpdate += OnFixedUpdate;
        GameManager.Instance.OnUpdate += OnUpdate;
    }

    private void OnDisable() 
    {
        GameManager.Instance.OnFixedUpdate -= OnFixedUpdate;
        GameManager.Instance.OnUpdate += OnUpdate;
    }

    private void OnUpdate()
    {
        Vector3 clampedPosition = transform.position;

        clampedPosition.x = Mathf.Clamp(clampedPosition.x, _bounds.MinX, _bounds.MaxX);
        clampedPosition.y = Mathf.Clamp(clampedPosition.y, _bounds.MinY, _bounds.MaxY);

        transform.position = clampedPosition;
    }

    private void OnFixedUpdate()
    {
        _acceleration = Input.acceleration;
        Vector2 circleVelocity = new Vector2(_acceleration.x, _acceleration.y);

        _rigidbody.velocity = circleVelocity * _speed;
    }
}

