using System;
using UnityEngine;

public class Tile : MonoBehaviour
{
    public event Action TileSpriteChanged;

    [SerializeField] private Sprite _rewardSprite;
    
    private BoxCollider2D _collider;
    private SpriteRenderer _spriteRenderer;

    private void Awake()
    {
        _collider = GetComponent<BoxCollider2D>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void OnTriggerEnter2D(Collider2D other) 
    {
        if(other.TryGetComponent(out Circle circle))
        {
            circle.PaintedOverTilesAmount++;

            _spriteRenderer.sprite = _rewardSprite;
            _collider.enabled = false;
            
            TileSpriteChanged?.Invoke();
        }
    }

    public void ActivateCollider()
    {
        _collider.enabled = true;
    }
        
    public void DisactivateCollider()
    {
        _collider.enabled = false;
    }

    public void SetColor(Color newColor)
    {
        _spriteRenderer.color = newColor;
        Debug.Log("Color setted" + newColor);
    }
}
