using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

[DefaultExecutionOrder(-2)]
public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get { return _instance; } }
    private static GameManager _instance;

    public event Action OnUpdate;
    public event Action OnFixedUpdate;
    public event Action OnGameStarted;
    public event Action OnGameOver;
    
    public Bounds Bounds { get; private set;}
    public int MaxAmountOfTiles { get; set; }
    [SerializeField] TilesController _tilesController;
    [SerializeField] Countdown _countdown;
    [SerializeField] Circle _circle;
    private Resizer resizer;

    private void Awake()
    {       
        if(_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        } 
        else 
        {
            _instance = this;
        }

        MaxAmountOfTiles = 18;

        Bounds = new Bounds();
        resizer = new Resizer(_circle, _tilesController, Bounds);
        resizer.Resize();
    }

    private void FixedUpdate() 
    {
        OnFixedUpdate?.Invoke();    
    }

    private void Update()
    {
        if(_tilesController.IsTilesOver)
        {
            OnGameOver?.Invoke();
        }

        OnUpdate?.Invoke();
    }

    private IEnumerator CountDown()
    {
        WaitForSeconds waitingTime = new WaitForSeconds(1f);
        int timeCount = 3;

        for(int i = timeCount; i > 0; i--)
        {
            _countdown.SetNumber(i);
            yield return waitingTime;
        }

        _countdown.HideCountdown();

        OnGameStarted?.Invoke();
    }

    public void QuitApplication()
    {
        Application.Quit();
    }

    public void StartGame()
    {
        StartCoroutine(CountDown());
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(0);
    }
}


