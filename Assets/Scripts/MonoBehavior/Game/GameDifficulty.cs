using UnityEngine;

public enum Difficulty
{
    Low,
    Middle,
    High
}

public class GameDifficulty : MonoBehaviour
{ 
    public Difficulty Level { get; set; }

    public void SetLowDifficulty()
    {
        Level = Difficulty.Low;
    }

    public void SetMiddleDifficulty()
    {
        Level = Difficulty.Middle;
    }

    public void SetHighDifficulty()
    {
        Level = Difficulty.High;
    }
}
