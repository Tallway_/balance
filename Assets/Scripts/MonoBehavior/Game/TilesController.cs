using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DefaultExecutionOrder(-1)]
public class TilesController : MonoBehaviour
{
    public bool IsTilesOver { get; private set; }
    public float TimeAmount 
    {  
        get
        {
            return _timeAmount;         
        } 
        set
        {
            if(value < 0f)
            {
                Debug.LogWarning("Incorrect time value! Try to input non negative float");
            }
            else
            {
                _timeAmount = value;
            }
        } 
    }
    [SerializeField] private List<GameObject> _tiles;

    private GameObject[] _shuffledTiles;
    private Color _activatedColor, _disactivatedColor;
    private float _timeAmount;
    private bool _isGameStarted, _isTileSpriteChanged;

    private void OnEnable() 
    {
        GameManager.Instance.OnGameStarted += OnGameStarted;
    }

    private void OnDisable() 
    {
        GameManager.Instance.OnGameStarted -= OnGameStarted;
    }

    private void OnGameStarted()
    {
        StartCoroutine(GameCycle());
    }

    private void Start()
    {
        _shuffledTiles = new GameObject[_tiles.Count];
        _activatedColor = new Color(255f, 255f, 255f, 1f);
        _disactivatedColor = new Color(255f, 255f, 255f, 50f / 255f);
        _timeAmount = _timeAmount == 0f ? 2f : _timeAmount;

        _tiles.CopyTo(_shuffledTiles);

        ShuffleTiles();
    }

    private void ShuffleTiles()
    {
        int firstRandomIndex = 0;
        int secondRandomIndex = 0;
        int tilesAmount = _shuffledTiles.Length;
        int shuffleCount = 20;

        for(int i = 0; i < shuffleCount; i++)
        {
            while(firstRandomIndex == secondRandomIndex)
            {
                firstRandomIndex = Random.Range(0, tilesAmount);
                secondRandomIndex = Random.Range(0, tilesAmount);
            } 

            SwapTiles(firstRandomIndex, secondRandomIndex);
            
            firstRandomIndex = 0;
            secondRandomIndex = 0;
        }
    }

    private void SwapTiles(int firstRandomIndex, int secondRandomIndex)
    {
        GameObject tempValue = _shuffledTiles[firstRandomIndex];
        _shuffledTiles[firstRandomIndex] = _shuffledTiles[secondRandomIndex];
        _shuffledTiles[secondRandomIndex] = tempValue;
    }

    private IEnumerator GameCycle()
    {
        WaitForSeconds pauseTime = new WaitForSeconds(_timeAmount);

        for(int i = 0; i < _shuffledTiles.Length; i++)
        {
            Tile currentTile = _shuffledTiles[i].GetComponent<Tile>();

            currentTile.TileSpriteChanged += OnTileSpriteChanged;

            currentTile.ActivateCollider();
            currentTile.SetColor(_activatedColor);

            yield return Pause(pauseTime);

            if(!_isTileSpriteChanged)
            {
                currentTile.DisactivateCollider();
                currentTile.SetColor(_disactivatedColor);
            }

            currentTile.TileSpriteChanged -= OnTileSpriteChanged;
            _isTileSpriteChanged = false;
        }

        IsTilesOver = true;
    }

    private void OnTileSpriteChanged()
    {      
        _isTileSpriteChanged = true;
    }

    private IEnumerator Pause(WaitForSeconds timeAmount)
    {
        yield return timeAmount;
    }
}
