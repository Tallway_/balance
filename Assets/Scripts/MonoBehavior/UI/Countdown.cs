using TMPro;
using UnityEngine;

public class Countdown : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _number;
    private Color32 _hideColor = new Color32(255, 255, 255, 0);

    public void SetNumber(int number)
    {
        _number.text = number.ToString();
    }

    public void HideCountdown()
    {
        _number.color = _hideColor;
    }
}
