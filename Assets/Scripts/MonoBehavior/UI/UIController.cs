using UnityEngine;

public class UIController : MonoBehaviour
{
    private const string GameOver = nameof(GameOver);

    [SerializeField] private Animator _animator;

    private void OnEnable() 
    {
        GameManager.Instance.OnGameOver += OnGameOver;
    }

    private void OnDisable() 
    {
        GameManager.Instance.OnGameOver -= OnGameOver;
    }

    private void OnGameOver()
    {
        _animator.SetTrigger(GameOver);
    }
}
