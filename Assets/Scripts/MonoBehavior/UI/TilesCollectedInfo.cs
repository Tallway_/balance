using TMPro;
using UnityEngine;

public class TilesCollectedInfo : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _maxTilesAmount;
    [SerializeField] private TextMeshProUGUI _paintedOverTilesAmount;
    private Circle _circle;

    void Start()
    {
        _circle = FindObjectOfType<Circle>();
    }

    private void OnEnable() 
    {
        GameManager.Instance.OnGameOver += OnGameOver;
    }

    private void OnDisable() 
    {
        GameManager.Instance.OnGameOver -= OnGameOver;
    }

    private void OnGameOver()
    {
        _maxTilesAmount.text = (GameManager.Instance.MaxAmountOfTiles).ToString();
        _paintedOverTilesAmount.text = (_circle.PaintedOverTilesAmount).ToString();
    }
}
