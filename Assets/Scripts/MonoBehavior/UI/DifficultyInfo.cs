using TMPro;
using UnityEngine;

public class DifficultyInfo : MonoBehaviour
{   
    [SerializeField] private TextMeshProUGUI _difficultyText;
    [SerializeField] private GameDifficulty _gameDifficulty;
    private string _difficulty;

    private void OnEnable() 
    {
        GameManager.Instance.OnGameOver += OnGameover;
    }

    private void OnDisable() 
    {
        GameManager.Instance.OnGameOver -= OnGameover;
    }

    private void OnGameover()
    {
        switch(_gameDifficulty.Level)
        {
            case Difficulty.Low:
                _difficulty = "Low";
                break;

            case Difficulty.Middle:
                _difficulty = "Middle";
                break;

            case Difficulty.High:
                _difficulty = "High";
                break;               
        }

        _difficultyText.text = _difficulty;
    }
}
