using UnityEngine;

public class Resizer
{
    private const float TargetAspect = 21f / 9f;

    private Circle _circle;
    private TilesController _tilesController;
    private Bounds _bounds;
    private float currentDeviceAspect, coefficient;

    public Resizer(Circle circle, TilesController tilesController, Bounds bounds)
    {
        _circle = circle;
        _tilesController = tilesController;
        _bounds = bounds;
    }

    public void Resize()
    {
        Resolution deviceResolution = Screen.currentResolution;

        //currentDeviceAspect = (float)deviceResolution.width / (float)deviceResolution.height;
        currentDeviceAspect = 2732f / 2048f;
        coefficient = currentDeviceAspect / TargetAspect;

        ResizeCircle();
        ResizeBounds();
        ResizeTiles();
    }

    private void ResizeTiles()
    {      
        Vector3 scale = _tilesController.transform.localScale;
        scale *= coefficient;
        _tilesController.transform.localScale = scale;
    }

    private void ResizeBounds()
    {
        _bounds.MinX *= coefficient;
        _bounds.MaxX *= coefficient;
        _bounds.MinY *= coefficient;
        _bounds.MaxY *= coefficient;
    }

    private void ResizeCircle()
    {
        Vector3 scale = _circle.transform.localScale;
        scale *= coefficient;
        _circle.transform.localScale = scale;
    }
}
